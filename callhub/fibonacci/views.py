# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from . import main_prog
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import csrf_protect
# Create your views here.

def index(request):
	return HttpResponse(render(None, "fibonacci/index.html")) 

def fibo(request):
	n= int(float(request.GET.get('n_value')))
	res,time_taken=main_prog.nthelement(n)
	return HttpResponse("The value of %s element is: %s calculated in %s seconds"% (n,res,time_taken))